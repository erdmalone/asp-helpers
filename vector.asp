<%
' Vector class.
'   Acts as a variable-length array that allows consumers to dynamically
'     add and remove items without having to worry about manually resizing
'     the collection or having to predetermine the necessary size.
Class Vector
    Private mArray
    Private mLength

    Private Sub Class_Initialize
        mArray  = Array()
        mLength = 0
    End Sub

    Private Sub Class_Terminate
        Erase mArray
        mLength = -1
    End Sub

    Public Property Get Length
        Length = mLength
    End Property

    ' Vector.Iterable property.
    '   Exposes the underlying array for use in iteration.
    '   eg. For Each loops.
    Public Property Get Iterable
        Iterable = mArray
    End Property

    Public Function FillFromArray( Arr, PreserveArray )
        If Not PreserveArray Then
            mArray  = Array()
            mLength = 0
        End If

        For Each Item In Arr
            Append( Item )
        Next
    End Function

    ' Append method.
    '   Adds an item to the vector, resizing as necessary.
    Public Sub Append( Item )
        If UCase( TypeName( Item ) ) = "VECTOR" Then
            For Each i In Item.Iterable
                mLength  = mLength + 1
                If mLength > UBound( mArray ) Then
                    ReDim Preserve mArray( UBound( mArray ) + 1 )
                End If
                mArray( mLength - 1 ) = i
            Next
        Else
            mLength = mLength + 1
            If mLength > UBound( mArray ) Then
                ReDim Preserve mArray( UBound( mArray ) + 1 )
            End If
            mArray( mLength - 1 ) = Item
        End If
    End Sub

    ' Remove method.
    '   Removes an item from the vector, resizing as necessary.
    '   Item MUST exist in the vector before removal.  If not
    '     found, nothing will happen.
    Public Sub Remove( Item )
        Idx = IndexOf( Item )
        If Idx <> -1 Then
            Dim i, j
            i = 0
            j = 0
            mArray( Idx ) = Null
            newArray = Array()

            For i = 0 To mLength - 1
                If Not IsNull( mArray( i ) ) Then
                    If j > UBound( newArray ) Then
                        ReDim Preserve newArray( UBound( newArray ) + 1 )
                    End If

                    newArray( j ) = mArray( i )
                    j = j + 1
                End If
            Next

            mArray  = newArray
            mLength = j
        End If
    End Sub

    ' RemoveLast method.
    '   Removes the last item in the vector, resizing as necessary.
    '   Vector must have at least one item to remove.
    Public Sub RemoveLast()
        Idx = mLength - 1
        If Idx <> -1 Then
            Dim i, j
            i = 0
            j = 0
            mArray( Idx ) = Null
            newArray = Array()

            For i = 0 To mLength - 1
                If Not IsNull( mArray( i ) ) Then
                    If j > UBound( newArray ) Then
                        ReDim Preserve newArray( UBound( newArray ) + 1 )
                    End If

                    newArray( j ) = mArray( i )
                    j = j + 1
                End If
            Next

            mArray  = newArray
            mLength = j
        End If
    End Sub

    ' RemoveFirst method.
    '   Removes the first item in the vector, resizing as necessary.
    '   Vector must have at least one item to remove.
    Public Sub RemoveFirst()
        Idx = 0
        If Idx <> -1 And mLength > 0 Then
            Dim i, j
            i = 0
            j = 0
            mArray( Idx ) = Null
            newArray = Array()

            For i = 0 To mLength - 1
                If Not IsNull( mArray( i ) ) Then
                    If j > UBound( newArray ) Then
                        ReDim Preserve newArray( UBound( newArray ) + 1 )
                    End If

                    newArray( j ) = mArray( i )
                    j = j + 1
                End If
            Next

            mArray  = newArray
            mLength = j
        End If
    End Sub

    ' RemoveAt method.
    '   Removes an item at the specified index, resizing as necessary.
    '   If the index is out of bounds, the method will do nothing.
    Public Sub RemoveAt( Index )
        If Index < mLength And Index >= 0 Then
            Dim i, j
            i = 0
            j = 0
            mArray( Index ) = Null
            newArray = Array()

            For i = 0 To mLength - 1
                If Not IsNull( mArray( i ) ) Then
                    If j > UBound( newArray ) Then
                        ReDim Preserve newArray( UBound( newArray ) + 1 )
                    End If

                    newArray( j ) = mArray( i )
                    j = j + 1
                End If
            Next

            mArray  = newArray
            mLength = j
        End If
    End Sub

    ' IndexOf method.
    '   Attempts to find the index at which the item exists.
    '   If no matching item is found, the function will return
    '     -1.
    Public Function IndexOf( Item )
        Index = -1
        For i = 0 To mLength - 1
            If mArray( i ) = Item Then
                Index = i
            End If
        Next

        IndexOf = Index
    End Function

    ' ItemAt method.
    '   Attempts to pull the item at the supplied index.
    '   If the index is out of bounds, the function will
    '     return Null.
    Public Function ItemAt( Index )
        If Index >= mLength Or Index <= ( -1 * mLength ) Then
            ItemAt = Null
        ElseIf Index < 0 Then
            ItemAt = mArray( mLength + Index )
        Else
            ItemAt = mArray( Index )
        End If
    End Function

    ' SetItemAt sub.
    '   Attempts to set the item at the specified index to
    '     the desired value.  If the index is out of bounds,
    '     the sub will exit, doing nothing.
    Public Sub SetItemAt( Index, Value )
        If Index >= mLength Or Index <= ( -1 * mLength ) Then
            Exit Sub
        ElseIf Index < 0 Then
            mArray( mLength + Index ) = Value
        Else
            mArray( Index ) = Value
        End If
    End Sub

    ' Replace sub.
    '   Sets any instance of the given value inside the array
    '     to the replacement value.
    Public Sub Replace( Value, Replacement )
        If mLength > 0 Then
            For VecRepIdx = 0 To UBound( mArray )
                If mArray( VecRepIdx ) = Value Then
                    mArray( VecRepIdx ) = Replacement
                End If
            Next
        End If
    End Sub
End Class
%>
