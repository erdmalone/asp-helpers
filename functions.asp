<%
' NullCoalesceList Function.
'  Coalesces the items within an array to a given value if they
'    are either NULL or EMPTY.
Function NullCoalesceList( list, value )
    Dim lst
    lst = list
    For NCLInc = 0 To UBound( list )
        lst( NCLInc ) = Ternary(                                   _
            IsNull( list( NCLInc ) ) Or IsEmpty( list( NCLInc ) ), _
            value,                                                 _
            list( NCLInc ) )
    Next
    NullCoalesceList = lst
End Function

' Minimum Function.
'  Returns the lower of the two provided values.
Function Minimum( x, y )
    Minimum = Ternary( x <= y, x, y )
End Function

' GetFStrFmtIndices Function.
'  Takes a given string and produces the indices at which FString
'    formatting directives are found.  Ignores escaped (preceded
'    with a backslash '\' character) braces, allowing users of the
'    FString function to more conveniently include them in their
'    usages of the function.
Function GetFStrFmtIndices( str )
    broken_str = StrToArray( str )
    open_idxs  = Array()
    clse_idxs  = Array()
    j          = 0
    k          = 0
    For i = 0 To UBound( broken_str )
        If i = 0 Then
            If broken_str( i ) = "{" Then
                If j > UBound( open_idxs ) Then
                    ReDim Preserve open_idxs( UBound( open_idxs ) + 1 )
                End If
                open_idxs( j ) = i
                j = j + 1
            ElseIf broken_str( i ) = "}" Then
                If k > UBound( clse_idxs ) Then
                    ReDim Preserve clse_idxs( UBound( clse_idxs ) + 1 )
                End If
                clse_idxs( k ) = i
                k = k + 1
            End If
        Else
            If broken_str( i - 1 ) <> "\" Then
                If broken_str( i ) = "{" Then
                    If j > UBound( open_idxs ) Then
                        ReDim Preserve open_idxs( UBound( open_idxs ) + 1 )
                    End If
                    open_idxs( j ) = i
                    j = j + 1
                ElseIf broken_str( i ) = "}" Then
                    If k > UBound( clse_idxs ) Then
                        ReDim Preserve clse_idxs( UBound( clse_idxs ) + 1 )
                    End If
                    clse_idxs( k ) = i
                    k = k + 1
                End If
            End If
        End If
    Next

    GetFStrFmtIndices = Array( open_idxs, clse_idxs )
End Function

' CommaSeparate Function.
'  Takes an array of values and builds a comma separated string
'    out of it.
Function CommaSeparate( values, endwithcomma )
    ret_val = ""
    For Each value In values
        ret_val = ret_val & value & ", "
    Next

    ret_val = Trim( ret_val )

    ' If we don't want the ending comma, then axe the last comma, else
    '  just leave it in its trimmed form.
    If Not endwithcomma Then
        ret_val = Left( ret_val, Len( ret_val ) - 1 )
    End If

    CommaSeparate = ret_val
End Function

' InferADOParamType Function.
'  Uses the type name of a given value to infer the most
'    appropriate ADO parameter type (adVarChar, etc) for
'    use in parameterized queries.
Function InferADOParamType( value )
    Set IADOPTTypeMap = Server.CreateObject( "Scripting.Dictionary" )

    IADOPTTypeMap.Add "LONG",    3
    IADOPTTypeMap.Add "INTEGER", 3
    IADOPTTypeMap.Add "DATE",    7
    IADOPTTypeMap.Add "SINGLE",  4
    IADOPTTypeMap.Add "DOUBLE",  5

    If IADOPTTypeMap.Exists( UCase( TypeName( value ) ) ) Then
        InferADOParamType = IADOPTTypeMap( UCase( TypeName( value ) ) )
    Else
        InferADOParamType = 200 ' If we can't infer the type, default to adVarChar.
    End If
End Function

' BuildQueryInputParams Function.
'  Using a list (either array or vector) of names and values,
'    this function will build a set of parameters in the format
'    expected by the ExecuteQueryWithParams function.  Note that
'    the types of collections must match, for now.  Also, only
'    builds adParamInput parameters, others aren't supported by
'    this function.
Function BuildQueryInputParams( names, values )
    If IsArray( names ) And IsArray( values ) Then
        If UBound( names ) <> UBound( values ) Then
            BuildQueryInputParams = Null
        Else
            ReDim BQIPVec( UBound( names ) )

            For i = 0 To UBound( names )
                BQIPParamType = InferADOParamType( values( i ) )
                BQIPParamSize = Ternary( BQIPParamType = 200, 128, 64 )

                BQIPVec( i ) = Array( _
                    names( i ), BQIPParamType, &H0001, BQIPParamSize, values( i ) )
            Next

            BuildQueryInputParams = BQIPVec
        End If
    ElseIf TypeName( names  ) = "Vector" And _
           TypeName( values ) = "Vector" Then
        If names.Length <> values.Length Then
            BuildQueryInputParams = Null
        Else
            ReDim BQIPVec( names.Length - 1 )

            For i = 0 To names.Length - 1
                BQIPParamType = InferADOParamType( values.ItemAt( i ) )
                BQIPParamSize = Ternary( BQIPParamType = 200, 128, 64 )

                BQIPVec( i ) = Array( _
                    names.ItemAt( i ), BQIPParamType, &H0001, BQIPParamSize, values.ItemAt( i ) )
            Next

            BuildQueryInputParams = BQIPVec
        End If
    Else
        BuildQueryInputParams = Null
    End If
End Function

' FString Function.
'  Likely my magnum opus.  This function takes in a string of the following format:
'      "stuff {format_to_be_evaluated}"
'    and actually executes the statements inside the braces, using the results to
'    format the provided string.  This should make dynamically formatting strings far,
'    FAR easier both in terms of execution AND readability.
'  Note that there may be limitations I haven't yet found and that the implementation
'    IS naive, meaning it'll execute ANY code put inside braces, so you can easily
'    and discreetly hose things if you aren't careful.
Function FString( str )
    braces = CharCount( "{", str )
    If braces < 1 Then
        FString = str
    Else ' FString detected formatting directives.
        directives = GetFStrFmtIndices( str )
        open_idxs  = directives( 0 )
        clse_idxs  = directives( 1 )

        ' We need to fail (return the source string) if either
        '  we couldn't pull the open and close brace indices OR
        '  there are an unequal number of them in the string.
        If UBound( open_idxs ) = -1 Or UBound( clse_idxs ) = -1 Then
            FString = str
        ElseIf UBound( open_idxs ) <> UBound( clse_idxs ) Then
            FString = str
        Else
            Dim i
            Set formats = Server.CreateObject( "Scripting.Dictionary" )
            fstr_eval   = ""
            i           = 0

            ' First, we slice out the format strings, putting them in a
            '  dictionary so duplicates don't interfere with latter parts.
            For i = 0 To UBound( open_idxs )
                tmp_fmt = Slice( str, open_idxs( i ) + 1, clse_idxs( i ) - 1 )

                If Not formats.Exists( "{" & tmp_fmt & "}" ) Then
                    formats.Add "{" & tmp_fmt & "}", "fstr_eval = " & tmp_fmt
                End If
            Next

            ' Copy the string to be formatted so we don't mess with the original.
            rtemp = str

            For Each fmt In formats
                ' Each formatting statement gets executed as if it were code, storing the
                '  value of the statement into fstr_eval.  Then, we replace the format
                '  tags with the results of the execution to provide the final result.
                Execute formats( fmt )
                rtemp = Replace( rtemp, fmt, fstr_eval )
            Next

            ' Lastly, we return the string with escaped curly braces rendered into their
            '  normal counterparts.
            FString = Replace( Replace( rtemp, "\}", "}" ), "\{", "{" )
        End If
    End If
End Function

' InStrAll Function.
'  Functions similarly to the native InStr() function, except
'    it returns the indices of ALL instances of a given character,
'    instead of just the first one.
Function InStrAll( str, char )
    If InStr( str, char ) > 0 And UCase( TypeName( str ) ) = "STRING" Then
        Dim ISAArr
        sz     = CharCount( char, str )
        st_arr = StrToArray( str )
        j      = 0

        ReDim ISAArr( sz - 1 )

        For i = 0 To UBound( st_arr )
            If st_arr( i ) = char Then
                ISAArr( j ) = i
                j = j + 1
            End If
        Next

        InStrAll = ISAArr
    Else
        InStrAll = Null
    End If
End Function

' StrToArray Function.
'  Transforms a string into an array of characters
'    for convenient indexing and iteration.
Function StrToArray( str )
    Dim chars
    STALen = Len( str ) - 1
    ReDim chars( STALen )

    For i = 0 To STALen
        chars( i ) = Mid( str, i + 1, 1 )
    Next

    StrToArray = chars
End Function

' Slice Function.
'  Cuts up either an array or a string, returning values
'    between start and finish indices, inclusive.
'  Failure states simply return the passed object.
Function Slice( obj, start, finish )
    Select Case UCase( TypeName( obj ) )
        Case "STRING"
            If start > finish Then
                Slice = obj
            ElseIf start = finish Then
                StrArr = StrToArray( obj )
                Slice  = StrArr( start )
            Else
                SliceRet = ""
                StrArr   = StrToArray( obj )

                For i = start To finish
                    SliceRet = SliceRet & StrArr( i )
                Next

                Slice = SliceRet
            End If
        Case "VARIANT()"
            If start > finish Then
                Slice = obj
            ElseIf start = finish Then
                Slice = obj( start )
            Else
                Dim RetArr
                sz = finish - start
                j  = 0
                ReDim RetArr( sz )

                For i = start To finish
                    RetArr( j ) = obj( i )
                    j = j + 1
                Next

                Slice = RetArr
            End If
        Case Else
            Slice = obj
    End Select
End Function

' InArray Function.
'  Tests if a provided value is in a provided array.
Function InArr( value, arr )
    If IsArray( arr ) Then
        InArr = False
        For Each val in arr
            If val = value Then
                InArr = True
                Exit For
            End If
        Next
    Else
        InArr = False
    End If
End Function

' CharCount Function.
'  Counts the number of times a given character appears
'    in a string.
Function CharCount( char, str )
    CharCount = Len( str ) - Len( Replace( str, char, "" ) )
End Function

' IsEmptyStr Function.
'  Check is the string is an empty string ("").
'  Because I'm sick and tired of seeing
'    [If str & "" <> "" Then]
'    everywhere.
Function IsEmptyStr( str )
    If str & "" = "" Then
        IsEmptyStr = True
    Else
        IsEmptyStr = False
    End If
End Function

' Ternary Function.
'  If the condition passed as the first argument is true,
'    then if_true will be returned, else, if_false will
'    be returned.  Allows for simple, one-line conditionals.
Function Ternary( cond, if_true, if_false )
    If cond = True Then
        Ternary = if_true
    Else
        Ternary = if_false
    End If
End Function

' EmptyCoalesce Function.
'  If the value passed is null, empty, or empty string (""),
'    then it returns if_null.  Otherwise, it returns the
'    original value.
Function EmptyCoalesce( value, if_null )
    If UCase( TypeName( value ) ) = "STRING" Then
        If ( IsNull( value ) Or IsEmpty( value ) Or value = "" ) Then
            EmptyCoalesce = if_null
        Else
            EmptyCoalesce = value
        End If
    Else
        If ( IsNull( value ) Or IsEmpty( value ) ) Then
            EmptyCoalesce = if_null
        Else
            EmptyCoalesce = value
        End If
    End If
End Function

' ArrayConcat Function.
'  Joins two arrays together; the second array gets appended
'    to the end of the first.
Function ArrayConcat( arr1, arr2 )
    If LCase( TypeName( arr1 ) ) <> "variant()" Or _
       LCase( TypeName( arr2 ) ) <> "variant()" Then
        ArrayConcat = Nothing
    Else
        Dim ArrConcNewArray
        ArrConcNewArray = arr1

        For Each value In arr2
            Dim ArrConcNewLen
            ArrConcNewLen = UBound( ArrConcNewArray ) + 1
            ReDim Preserve ArrConcNewArray( ArrConcNewLen )
            ArrConcNewArray( ArrConcNewLen ) = value
        Next

        ArrayConcat = ArrConcNewArray
    End If
End Function

' RangeRepeat Function.
'  Returns an array with the providev value repeated n times.
Function RangeRepeat( value, n )
    If n < 1 Then
        RangeRepeat = Nothing
    Else
        Dim i, arr
        ReDim arr( n - 1 )
        For i = 0 To n - 1
            arr( i ) = value
        Next

        RangeRepeat = arr
    End If
End Function

' Max Function.
'  Returns the highest value of either a or b.
Function Max( a, b )
    If a > b Then
        Max = a
    Else
        Max = b
    End If
End Function

' ExecuteQueryWithParams Function.
'  Executes a parameterized query string using the provided
'    connection string or object and a list of parameters.
'  N.B: Parameters MUST be a 2D array, with each entry
'    in the following format:
'
'  0: The name of the parameter (string)
'  1: The ADO data type of the parameter (one of the constants above)
'  2: The direction in which the data is going (one of the constants above)
'  3: The size of the parameter's value. (integer)
'  4: The value that will be passed into this parameter, to be used in the
'       query.
Function ExecuteQueryWithParams( Conn, Query, Params )
    If LCase( TypeName( Conn ) ) = "string" Then
        Set Connection = CreateObject( "ADODB.Connection" )
        Set Command    = CreateObject( "ADODB.Command"    )
        Connection.Open( Conn )

        Command.CommandType      = adCmdText
        Command.ActiveConnection = Connection
        Command.CommandText      = Query

        For Each Param In Params
            ParamName  = Param( 0 )
            DType      = Param( 1 )
            Direction  = Param( 2 )
            ParamSize  = Param( 3 )
            ParamValue = Param( 4 )

            Set Parameter = Command.CreateParameter( ParamName, DType, Direction, ParamSize )

            Command.Parameters.Append( Parameter )
            Parameter.Value = ParamValue
        Next

        Set Results                = Command.Execute()
        Set ExecuteQueryWithParams = Results
    ElseIf LCase( TypeName( Conn ) ) = "connection" Then
        Set Command = CreateObject( "ADODB.Command" )

        Command.CommandType      = adCmdText
        Command.ActiveConnection = Conn
        Command.CommandText      = Query

        For Each Param In Params
            ParamName  = Param( 0 )
            DType      = Param( 1 )
            Direction  = Param( 2 )
            ParamSize  = Param( 3 )
            ParamValue = Param( 4 )

            Set Parameter = Command.CreateParameter( ParamName, DType, Direction, ParamSize )

            Command.Parameters.Append( Parameter )
            Parameter.Value = ParamValue
        Next

        'Stop
        Set Results                = Command.Execute()
        Set ExecuteQueryWithParams = Results
    Else
        ExecuteQueryWithParams = Null
    End If
End Function

' ResultSetHasData Function.
'  Checks if a given resultset has any data by
'    checking the EOF & BOF flags.  If both are
'    true, the resultset has no data to provide.
Function ResultSetHasData( ResultSet )
    If ( ResultSet.EOF And ResultSet.BOF ) Then
        ResultSetHasData = False
    Else
        ResultSetHasData = True
    End If
End Function

' ExecuteQuery Function.
'  Executes a provided query string using the
'    provided connection string.
Function ExecuteQuery( ConnString, Query )
    Set Connection = CreateObject( "ADODB.Connection" )
    Connection.Open( ConnString )
    Set ResultSet = Connection.Execute( Query )

    Set ExecuteQuery = ResultSet
End Function

' StringFormat Function.
'  Takes in a string of the format
'    "stuff {0} junk {1}"
'  as well as an array of values
'    eg. Array(3, "tomato")
'  to produce a string of the desired format without
'  having to do a large amount of string concatenation.
Function StringFormat(FmtString, Args())
    Dim Value, CurArg
    StringFormat = FmtString

    CurArg = 0
    For Each Value In Args
        StringFormat = Replace(StringFormat, "{" & CurArg & "}", Value)
        CurArg = CurArg + 1
    Next
End Function
%>
